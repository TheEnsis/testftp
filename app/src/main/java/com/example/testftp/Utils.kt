package com.example.testftp

import org.apache.commons.net.ftp.FTP
import org.apache.commons.net.ftp.FTPClient
import java.io.*


/**
 * @param ftpClient      FTPclient object
 * @param remoteFilePath FTP server file path
 * @param downloadFile   local file path where you want to save after download
 * @return status of downloaded file
 */
private fun downloadSingleFile(ftpClient: FTPClient, remoteFilePath: String, downloadFile: File): Boolean {
    val parentDir = downloadFile.parentFile
    if (!parentDir.exists())
        parentDir.mkdir()
    var outputStream: OutputStream? = null
    try {
        outputStream = BufferedOutputStream(FileOutputStream(downloadFile))
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
        return ftpClient.retrieveFile(remoteFilePath, outputStream)
    } catch (ex: Exception) {
        ex.printStackTrace()
    } finally {
        ftpClient.logout()
        ftpClient.disconnect()
        if (outputStream != null) {
            try {
                outputStream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }
    return false
}

/**
 * @param ftpClient      FTPclient object
 * @param localDIR   local file path where you want to save after download
 * @return status of downloaded file
 */
private fun downloadMultipleFiles(ftpClient: FTPClient, localDIR: String): Boolean {
    if (!File(localDIR).exists())
        File(localDIR).mkdir()
    val mFileArray = ftpClient.listFiles()
    try {
        if (mFileArray != null && mFileArray.isNotEmpty()) {
            for (fl in mFileArray) {
                if (!fl.isFile) {
                    continue
                }
                println(fl.name)
                val out: OutputStream
                out = FileOutputStream(localDIR + "/" + fl.name)
                ftpClient.retrieveFile(fl.name, out)
                out.close()
//                    if (fl.name.takeLast(3) == "png" || fl.name.takeLast(3) == "jpg" || fl.name.takeLast(3) == "jpeg") fetchImage(
//                        Environment.getExternalStorageDirectory().path + fl.name
//                    )
            }
        }
        return true
//        val parentDir = downloadFile.parentFile
//        if (!parentDir.exists())
//            parentDir.mkdir()
//        var outputStream: OutputStream? = null
//        try {
//            outputStream = BufferedOutputStream(
//                FileOutputStream(
//                    downloadFile
//                )
//            )
//            ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
//            return ftpClient.retrieveFile(remoteFilePath, outputStream)
    } catch (ex: Exception) {
        ex.printStackTrace()
    } finally {
        ftpClient.logout()
        ftpClient.disconnect()
//            if (outputStream != null) {
//                try {
//                    outputStream.close()
//                } catch (e: IOException) {
//                    e.printStackTrace()
//                }
//
//            }
    }
    return false
}
