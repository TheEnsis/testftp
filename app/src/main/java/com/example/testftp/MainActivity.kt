package com.example.testftp

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaScannerConnection
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import org.apache.commons.net.ftp.FTP
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPReply
import java.io.*
import java.net.InetAddress
import java.net.SocketException
import java.net.UnknownHostException
import android.os.StrictMode
import android.widget.Toast
import androidx.core.content.ContextCompat
import java.io.File.separator


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        checkPermission({ initInstance() }, 1122)

    }

    private val name = "pushcodex.com"
    private val user = "signage@pushcodex.com"
    private val pass = "signage"
    private val listFolder = mutableListOf<String>()
    private val pathToSave = Environment.getExternalStorageDirectory().path + "/htdocs"

    private fun initInstance() {
        val ftp = connectingWithFTP(name, user, pass)
//        val x = ftp?.let {
//            downloadSingleFile(
//                it,
//                "/PA2_1553054374_99492.jpg",
//                File(Environment.getExternalStorageDirectory().path, "PA2_1553054374_99492.jpg")
//            )
//        }
        if (!File(pathToSave).exists()) File(pathToSave).mkdir()
        if (listFolder.isNotEmpty()) {
            listFolder.forEach { name ->
                if (!File("$pathToSave/$name").exists()) File("$pathToSave/$name").mkdir()
                ftp?.let {
                    downloadDirectory(it, "/$name", "", pathToSave)
                }
            }
        }
        val connection = ftp?.let {
            downloadMultipleFiles(it, pathToSave)
        }
        Log.i("MAIN", "Download Status : " + connection.toString())
        Toast.makeText(this, "Download Status : " + connection.toString(), Toast.LENGTH_SHORT).show()

        startActivity(Intent(this, WebActivity::class.java))
    }

    /**
     * @param ip
     * @param userName
     * @param pass
     */
    private fun connectingWithFTP(ip: String, userName: String, pass: String): FTPClient? {
        var status = false
        try {
            val mFtpClient = FTPClient()
            mFtpClient.connectTimeout = 10 * 1000
            mFtpClient.connect(InetAddress.getByName(ip))
            status = mFtpClient.login(userName, pass)
            Log.e("isFTPConnected", status.toString())
            if (FTPReply.isPositiveCompletion(mFtpClient.replyCode)) {
                mFtpClient.setFileType(FTP.ASCII_FILE_TYPE)
                mFtpClient.enterLocalPassiveMode()
                val mFileArray = mFtpClient.listFiles()
                Log.e("Size : ", mFileArray.size.toString())
                mFileArray.forEach { Log.e("MAIN", it.name) }

                mFileArray.map { it.name }.filterNot { it.contains(".") }.toCollection(listFolder)
                Log.e("List", listFolder.toString())
            }
            return mFtpClient
        } catch (e: SocketException) {
            e.printStackTrace()
        } catch (e: UnknownHostException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    /**
     * Download a whole directory from a FTP server.
     * @param ftpClient an instance of org.apache.commons.net.ftp.FTPClient class.
     * @param parentDir Path of the parent directory of the current directory being
     * downloaded.
     * @param currentDir Path of the current directory being downloaded.
     * @param saveDir path of directory where the whole remote directory will be
     * downloaded and saved.
     * @throws IOException if any network or IO error occurred.
     */
    @Throws(IOException::class)
    fun downloadDirectory(ftpClient: FTPClient, parentDir: String, currentDir: String, saveDir: String) {
        var dirToList = parentDir
        if (currentDir != "") {
            dirToList += "/$currentDir"
        }

        val subFiles = ftpClient.listFiles(dirToList)

        if (subFiles != null && subFiles.isNotEmpty()) {
            for (aFile in subFiles) {
                val currentFileName = aFile.name
                if (currentFileName == "." || currentFileName == "..") {
                    // skip parent directory and the directory itself
                    continue
                }
                var filePath = ("$parentDir/$currentDir/$currentFileName")
                if (currentDir == "") {
                    filePath = "$parentDir/$currentFileName"
                }

                var newDirPath = (saveDir + parentDir + separator + currentDir + separator + currentFileName)
                if (currentDir == "") {
                    newDirPath = (saveDir + parentDir + separator + currentFileName)
                }

                if (aFile.isDirectory) {
                    // create the directory in saveDir
                    val newDir = File(newDirPath)
                    val created = newDir.mkdirs()
                    if (created) {
                        println("CREATED the directory: $newDirPath")
                    } else {
                        println("COULD NOT create the directory: $newDirPath")
                    }

                    // download the sub directory
                    downloadDirectory(ftpClient, dirToList, currentFileName, saveDir)
                } else {
                    // download the file
                    val success = downloadSingleFile(ftpClient, filePath, newDirPath)
                    if (success) {
                        println("DOWNLOADED the file: $filePath")
                    } else {
                        println("COULD NOT download the file: $filePath")
                    }
                }
            }
        }
    }

    /**
     * Download a single file from the FTP server
     * @param ftpClient an instance of org.apache.commons.net.ftp.FTPClient class.
     * @param remoteFilePath path of the file on the server
     * @param savePath path of directory where the file will be stored
     * @return true if the file was downloaded successfully, false otherwise
     * @throws IOException if any network or IO error occurred.
     */
    @Throws(IOException::class)
    fun downloadSingleFile(ftpClient: FTPClient, remoteFilePath: String, savePath: String): Boolean {
        val downloadFile = File(savePath)

        val parentDir = downloadFile.parentFile
        if (!parentDir.exists()) {
            parentDir.mkdir()
        }

        val outputStream = BufferedOutputStream(FileOutputStream(downloadFile))
        try {
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE)
            return ftpClient.retrieveFile(remoteFilePath, outputStream)
        } catch (ex: IOException) {
            throw ex
        } finally {
            outputStream.close()
        }
    }

    /**
     * @param ftpClient      FTPclient object
     * @param localDIR   local file path where you want to save after download
     * @return status of downloaded file
     */
    private fun downloadMultipleFiles(ftpClient: FTPClient, localDIR: String): Boolean {
        if (!File(localDIR).exists())
            File(localDIR).mkdir()
        val mFileArray = ftpClient.listFiles().filter { it.name.contains(".") }
        try {
            if (mFileArray.isNotEmpty()) {
                for (fl in mFileArray) {
                    if (!fl.isFile) {
                        continue
                    }
                    println(fl.name)
                    val out: OutputStream
                    out = FileOutputStream(localDIR + "/" + fl.name)
                    val retrieve = ftpClient.retrieveFile(fl.name, out)
                    out.close()
                    println("DOWNLOADED the file: ${fl.name} $retrieve")
                }
            }
            return true
        } catch (ex: Exception) {
            ex.printStackTrace()
        } finally {
            ftpClient.logout()
            ftpClient.disconnect()
        }
        return false
    }

    private fun checkPermission(unit: () -> Unit, code: Int) {
        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            unit()
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), code)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1122 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    initInstance()
                else
                    Toast.makeText(this, "Please grant storage permission", Toast.LENGTH_SHORT).show()
            }
            else -> return
        }
    }

    private fun Context.fetchImage(imageDetail: String) {
        MediaScannerConnection.scanFile(
            this,
            arrayOf(imageDetail),
            arrayOf("images/*")
        ) { path, uri -> Log.i("ScanCompleted", "Scanned $path:") }
    }
}