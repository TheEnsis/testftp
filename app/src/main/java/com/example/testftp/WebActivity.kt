package com.example.testftp

import android.annotation.SuppressLint
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.web_activity.*
import java.io.*
import android.widget.FrameLayout
import android.graphics.BitmapFactory
import android.view.WindowManager
import android.widget.RelativeLayout

class WebActivity : AppCompatActivity() {

    private val baseUrl =
        Environment.getExternalStorageDirectory().path + "/htdocs"
    private val indexHtml = "/index.html"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.web_activity)

        initInstance()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initInstance() {
//        myWebView.webChromeClient = MyChrome()
//        myWebView.webViewClient = WebViewClient()
        myWebView.settings.javaScriptEnabled = true
//        myWebView.settings.allowFileAccess = true
        myWebView.webView.setInitialScale(100)
//        myWebView.loadDataWithBaseURL(baseUrl, readAssetFileAsString("/demo.html"), "text/html", "UTF-8", null)
        myWebView.loadUrl("file://$baseUrl$indexHtml")
    }

    private fun readAssetFileAsString(sourceHtmlLocation: String): String {
        //Read text from file
        val file = File(baseUrl + sourceHtmlLocation)
        val fileString = file.readText()
        Log.e("HTML", fileString)
        return fileString
    }
}
