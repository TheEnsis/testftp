package com.example.testftp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_select.*

class SelectActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select)


        btnDownload.setOnClickListener { startActivity(Intent(this, MainActivity::class.java)) }
        btnOpenWebView.setOnClickListener { startActivity(Intent(this, WebActivity::class.java)) }
    }
}
